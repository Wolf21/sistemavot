package Negocio;

import Entidad.*;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Random;
import ufps.util.colecciones_seed.*;
import ufps.util.varios.ArchivoLeerURL;

public class SistemaVotacion {

    private ListaCD<Departamento> dptos = new ListaCD();
    private ListaCD<Persona> personas = new ListaCD();
    private Cola<Notificacion> notificaciones = new Cola();

//CONSTRUCTOR
    public SistemaVotacion(String urlDpto, String urlMuni, String urlPer, String urlCen) {
        crearDepartamentos(urlDpto);
        crearMunicipios(urlMuni);
        crearPersonas(urlPer);
        crearCentros(urlCen);

    }

    public void asignarJurados() {
        //ESTA LÓGICA ES A NIVEL MUY MUY MUY GENERAL
        for (Persona persona : this.personas) {
            Persona dato = getPersonaAleatorio();
            if (!dato.isEsJurado()) {
                int idInscripcion = dato.getId_Municipio_inscripcion();
                Municipio muni = this.getMunicipio(idInscripcion);
                CentroVotacion centro = muni.getCentroAleatorio();
                Mesa mesa = centro.getMesaAleatoria();
                if (mesa.asignarJurado(dato)) {
                    Notificacion nueva = new Notificacion("", muni.getNombre(), crearFecha("2019-10-27"), dato);
                    nueva.crearNotificacion(centro, mesa);
                    this.notificaciones.enColar(nueva);

                }
            }
        }

    }

    private Persona getPersonaAleatorio() {
        int tam = this.personas.getTamanio() - 1;
        Random x = new Random();
        int posRandom = x.nextInt(tam);
        return this.personas.get(posRandom);
    }
 

    public boolean todosSonSufragantes() {
        for (Persona dato : personas) {
            if (!(dato.isEsJurado() || dato.isEsSufragante())) {
                return true;
            }
        }
        return false;
    }

    public void asignarSufrangantes() {
        while (!todosSonSufragantes()) {
            Persona dato = getPersonaAleatorio();
            if (!dato.isEsJurado() ) {
                int idInscripcion = dato.getId_Municipio_inscripcion();
                Municipio muni = this.getMunicipio(idInscripcion);
                CentroVotacion centro = muni.getCentroAleatorio();
                if (centro != null) {
                Mesa mesa = centro.getMesaAleatoria();
                    if (mesa.asignarSufragante(dato)) {
                        System.out.println("h");
                        Notificacion nueva = new Notificacion("", muni.getNombre(), crearFecha("2019-10-27"), dato);
                        nueva.crearNotificacion(centro, mesa);
                        this.notificaciones.enColar(nueva);
                    }
                }
            }

        }

    }

    private void crearDepartamentos(String url) {
        ArchivoLeerURL ur = new ArchivoLeerURL(url);
        Object v[] = ur.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String datos = v[i].toString();
            String datos1[] = datos.split(";");
            int id = Integer.parseInt(datos1[0]);
            String nombre = datos1[1];

            Departamento nuevo = new Departamento(id, nombre);
            this.dptos.insertarAlFinal(nuevo);
        }
    }

    private void crearMunicipios(String url) {
        ArchivoLeerURL ur = new ArchivoLeerURL(url);
        Object v[] = ur.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String dato = v[i].toString();
            String dato1[] = dato.split(";");
            int id_Dpto = Integer.parseInt(dato1[0]);
            int id_Muni = Integer.parseInt(dato1[1]);
            String nombre = dato1[2];

            Municipio nuevo = new Municipio(id_Muni, nombre);
            Departamento dpto = buscarDpto(id_Dpto);
            if (dpto != null) {
                dpto.getMunicipios().insertarAlFinal(nuevo);
            }
        }
    }

    private Departamento buscarDpto(int id) {
        for (Departamento dato : this.dptos) {
            if (dato.getId_dpto() == id) {
                return dato;
            }
        }
        return null;
    }

    private void crearPersonas(String url) {
        ArchivoLeerURL ur = new ArchivoLeerURL(url);
        Object v[] = ur.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String dato = v[i].toString();
            String dato1[] = dato.split(";");
            long cedula = Long.parseLong(dato1[0]);
            String nombre = dato1[1];
            LocalDateTime fecha = crearFecha(dato1[2]);
            short id_Mun = Short.parseShort(dato1[3]);
            String correo = dato1[4];

            Persona nueva = new Persona(cedula, nombre, fecha, id_Mun, correo);
            this.personas.insertarAlFinal(nueva);
        }
    }

    private LocalDateTime crearFecha(String fecha) {
        String dato[] = fecha.split("-");
        int anio = Integer.parseInt(dato[0]);
        int mes = Integer.parseInt(dato[1]);
        int dia = Integer.parseInt(dato[2]);

        return LocalDateTime.of(anio, mes, dia, 0, 0, 0);
    }

    private void crearCentros(String url) {
        ArchivoLeerURL ur = new ArchivoLeerURL(url);
        Object v[] = ur.leerArchivo();
        for (int i = 1; i < v.length; i++) {
            String dato = v[i].toString();
            String dato1[] = dato.split(";");
            int id = Integer.parseInt(dato1[0]);
            String nombre = dato1[1];
            String direccion = dato1[2];
            int id_Mun = Integer.parseInt(dato1[3]);
            int cantMesas = Integer.parseInt(dato1[4]);
            int maxSufragantes = Integer.parseInt(dato1[5]);

            Municipio muni = this.getMunicipio(id_Mun);
            if (muni != null) {
                CentroVotacion nuevo = new CentroVotacion(id_Mun, nombre, direccion, maxSufragantes);
                nuevo.crearMesas(cantMesas);
                muni.getCentros().insertarAlFinal(nuevo);
            }
        }
    }

    private Municipio getMunicipio(int id) {
        for (Departamento dato : this.dptos) {
            Municipio muni = dato.getMunicipio(id);
            if (muni != null) {
                return muni;
            }
        }
        return null;
    }

    public String getListadoDpto() {
        String msg = "";
        for (Departamento dato : this.dptos) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }

    public String getListadoPersonas() {
        String msg = "";
        for (Persona dato : this.personas) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }

    public String getListadoNotificaciones() {
        String msg = "";
        Cola<Notificacion> c = new Cola();
        while (!this.notificaciones.esVacia()) {
            msg += this.notificaciones.getInfoInicio()+"\n";
            c.enColar(this.notificaciones.deColar());
        }
        llenarCola(c);
        return msg;
    }

    public String impresion() {
        String msg = "";

        asignarJurados();
        asignarSufrangantes();
        msg += "" + getListadoNotificaciones();
//        msg += "" + getMesasAnuladas();
        return msg;
    }

    private void llenarCola(Cola<Notificacion> n) {
        while (!n.esVacia()) {
            this.notificaciones.enColar(n.deColar());
        }
    }

    private String getMesasAnuladas() {
        String msg = "";
        for (Departamento dato : this.dptos) {
            msg += dato.toString() + "\n";
        }
        return msg;
    }

    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    public void setDptos(ListaCD<Departamento> dptos) {
        this.dptos = dptos;
    }

    public ListaCD<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ListaCD<Persona> personas) {
        this.personas = personas;
    }

    public Cola<Notificacion> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(Cola<Notificacion> notificaciones) {
        this.notificaciones = notificaciones;
    }

}
