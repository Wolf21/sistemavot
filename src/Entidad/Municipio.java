package Entidad;

import ufps.util.colecciones_seed.ListaCD;

public class Municipio {

    private int id_municipio;

    private String nombre;
    
    private ListaCD<CentroVotacion> centros=new ListaCD();
    
    
//CONSTRUCTORES
    public Municipio() {
    }

    public Municipio(int id_municipio, String nombre) {
        this.id_municipio = id_municipio;
        this.nombre = nombre;
    }
    
    
//GET AND SET
    public int getId_municipio() {
        return id_municipio;
    }

    public void setId_municipio(int id_municipio) {
        this.id_municipio = id_municipio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
//GET AND SET DE LA LISTA DE CENTROS DE VOTACION
    public ListaCD<CentroVotacion> getCentros() {
        return centros;
    }

    public void setCentros(ListaCD<CentroVotacion> centros) {
        this.centros = centros;
    }

    @Override
    public String toString() {
        return "\n Municipio{" + "id_municipio=" + id_municipio + ", nombre=" + nombre + 
                "\n Mis centros son:"+this.getListadoCentro()+'}';
    }
    
    public String getListadoCentro(){
        String msg="";
        for(CentroVotacion dato: this.centros){
        msg+=dato.toString()+"\n";
        }
        return msg;
    }
  
    public CentroVotacion getCentroAleatorio(){
    
        int i= (int) (Math.random() * ((this.centros.getTamanio()-1)-0+1)+0);
        return this.centros.get(i);
    }
    
    
}
