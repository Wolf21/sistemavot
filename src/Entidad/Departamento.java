package Entidad;

import ufps.util.colecciones_seed.ListaCD;

public class Departamento {

    private int id_dpto;

    private String nombreDpto;
    
    private ListaCD<Municipio> municipios=new ListaCD();
    
    
//CONSTRUCTORES
    public Departamento() {
    }

    public Departamento(int id_dpto, String nombreDpto) {
        this.id_dpto = id_dpto;
        this.nombreDpto = nombreDpto;
    }
    
// GET AND SET 
    public int getId_dpto() {
        return id_dpto;
    }

    public void setId_dpto(int id_dpto) {
        this.id_dpto = id_dpto;
    }

    public String getNombreDpto() {
        return nombreDpto;
    }

    public void setNombreDpto(String nombreDpto) {
        this.nombreDpto = nombreDpto;
    }

    
//GET AND SET DE LA LISTA DE MUNICIPIOS     
    public ListaCD<Municipio> getMunicipios() {
        return municipios;
    }

    public void setMunicipios(ListaCD<Municipio> municipios) {
        this.municipios = municipios;
    }
    
    private String getListadoMunicipios(){
    String msg="";
    for(Municipio dato: getMunicipios())
        msg+=dato.toString()+"\n";
    return msg;
    
    }
    
    public Municipio getMunicipio(int id){
        for(Municipio muni: getMunicipios()){
        if(muni.getId_municipio()==id)
            return muni;
        }
        return null;            
    }
    
    
 
 //toSTRING

    @Override
    public String toString() {
         return "Departamento{" + "id_dpto=" + id_dpto + ", nombreDpto=" + nombreDpto +
                this.getListadoMunicipios()+"}\n\n";
    }
    
}
