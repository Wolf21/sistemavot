package Entidad;

import java.util.Random;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.colecciones_seed.Pila;

public class CentroVotacion {

    private int id_centro;

    private String nombreCentro;

    private String direccion;

    private Pila<Mesa> mesas = new Pila();

    private int cantidadSufragantes;

    //Constructores
    public CentroVotacion() {
    }

    public CentroVotacion(int id_centro, String nombreCentro, String direccion, int cantidadSufragantes) {
        this.id_centro = id_centro;
        this.nombreCentro = nombreCentro;
        this.direccion = direccion;
        this.cantidadSufragantes = cantidadSufragantes;
    }

    public void crearMesas(int cantidad) {
        if (cantidad < 1) {
            return;
        }
        for (int i = 0; i < cantidad; i++) {
            this.mesas.apilar(new Mesa(i));
        }
    }

    public String getListadoMesas() {
        Pila<Mesa> mesa = new Pila();
        String msg = "";
        while (!this.mesas.esVacia()) {
            msg += mesas.getTope();
            mesa.apilar(mesas.desapilar());
        }
        cargarPila(mesa);

        return msg;
    }

    private void cargarPila(Pila<Mesa> p){
            while (!p.esVacia()) {
            mesas.apilar(p.desapilar());
        }
    }
    
    //GET AND SET 
    public int getId_centro() {
        return id_centro;
    }

    public void setId_centro(int id_centro) {
        this.id_centro = id_centro;
    }

    public String getNombreCentro() {
        return nombreCentro;
    }

    public void setNombreCentro(String nombreCentro) {
        this.nombreCentro = nombreCentro;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getCantidadSufragantes() {
        return cantidadSufragantes;
    }

    public void setCantidadSufragantes(int cantidadSufragantes) {
        this.cantidadSufragantes = cantidadSufragantes;
    }

    //GET AND  SET DE LA PILA DE MESA
    public Pila<Mesa> getMesas() {
        return mesas;
    }

    public void setMesas(Pila<Mesa> mesas) {
        this.mesas = mesas;
    }

    @Override
    public String toString() {
        //Este proceso es de cuidado por que se debe sacar copia de la pila
        //Esto l4o deben hacer :(

        return "CentroVotacion{" + "id_centro=" + id_centro + ", nombreCentro=" + nombreCentro + ", direccion=" + direccion + ", cantidadSufragantes=" + cantidadSufragantes
                + "\n Mis mesas son:" + this.getListadoMesas() + '}';
    }

    public Mesa getMesaAleatoria() {
        
    int tam=this.mesas.getTamanio();
            Random x=new Random();
            int posRandom=x.nextInt(tam);
            Mesa datoPila= null;
            
            Pila<Mesa> aux= new Pila();
            while(posRandom >=0){
            datoPila= this.mesas.desapilar();
            aux.apilar(datoPila);
            posRandom--;
            }
            cargarPila(aux);
            return datoPila;
    }
    
    public Mesa mesasAnuladas(){
    return null;}

}
