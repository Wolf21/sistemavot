package Entidad;


import java.time.LocalDateTime;

public class Notificacion {

    private String nombreDpto;

    private String nombreMunicipio;

    private LocalDateTime fechaVotacion;
    
    private Persona persona;
    
    
//CONSTRUCTOR
    public Notificacion() {
    }

    public Notificacion(String nombreDpto, String nombreMunicipio, LocalDateTime fechaVotacion, Persona persona) {
        this.nombreDpto = nombreDpto;
        this.nombreMunicipio = nombreMunicipio;
        this.fechaVotacion = fechaVotacion;
        this.persona = persona;
    }
    
    

    public String getNombreDpto() {
        return nombreDpto;
    }

//GET AND SET 
    public void setNombreDpto(String nombreDpto) {
        this.nombreDpto = nombreDpto;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public LocalDateTime getFechaVotacion() {
        return fechaVotacion;
    }

    public void setFechaVotacion(LocalDateTime fechaVotacion) {
        this.fechaVotacion = fechaVotacion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    public String crearNotificacion(CentroVotacion c, Mesa m){
        String msg= asignarMensaje()+ "el municipio "+ this.nombreMunicipio+ ", en el centro " + c + "y en la mesa " + m;
        return msg;
    }
    
    
    
    private String asignarMensaje(){
        
        if(this.persona.isEsJurado())
            return "Usted ha sido designado como jurado de votacion el día 27 de octubre de 2019 en ";
        if(this.persona.isEsSufragante())
            return "Su puesto de votacion para las elecciones del día 27 de octubre de 2019 esta ubicado en ";
                    
        return "Usted se ha quedado sin puesto de votacion";
    }

    @Override
    public String toString() {
        return "Notificacion{" + "nombreDpto=" + nombreDpto + ", nombreMunicipio=" + nombreMunicipio + ", fechaVotacion=" + fechaVotacion + ", persona=" + persona + '}';
    }
    
    
    
    
    
}
