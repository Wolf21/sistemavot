package Entidad;

import java.time.LocalDateTime;
import ufps.util.colecciones_seed.Cola;

public class Mesa {

    private int id_mesa;
    private Cola<Persona> sufragantes=new Cola();
    private Persona[] jurados=new Persona[3];

//CONSTRUCTORES
    public Mesa() {
    }

    public Mesa(int id_mesa) {
        this.id_mesa = id_mesa;
    }
    
//GET AND SET 
    public int getId_mesa() {
        return id_mesa;
    }

    public void setId_mesa(int id_mesa) {
        this.id_mesa = id_mesa;
    }
   
//GET AND SET DE LA COLA DE PERSONAS SUFRAGANTES
    public Cola<Persona> getSufragantes() {
        return sufragantes;
    }

    public void setSufragantes(Cola<Persona> sufragantes) {
        this.sufragantes = sufragantes;
    }

//GET AND SET DEL VECTOR  DE PERSONAS JURADOS
    public Persona[] getJurados() {
        return jurados;
    }

    public void setJurados(Persona[] jurados) {
        this.jurados = jurados;
    }

    @Override
    public String toString() {
        return "Mesa{" + "id_mesa=" + id_mesa + '}';
    }
    
    public boolean asignarJurado(Persona p)
    {
    if(!p.esAptoParaJurado())
        return false;
    int i=buscarPosicionJurado();
    if(i!=-1)
        {
            this.jurados[i]=p;
            p.setEsJurado(true);
            p.setEsSufragante(true);
            return true;
        }
    return false;
    }
    
    private int buscarPosicionJurado()
    {
    for(int i=0;i<this.jurados.length;i++)
        {
            if(this.jurados[i]==null)
                return i;
        }
    return -1;
    }
    
    //corregir, es una cola y no un vector
    public boolean asignarSufragante(Persona p)
    {
//////    if(!p.esAptoParaVotar())
//////        return false;  
    
    p.setEsSufragante(true);
    this.sufragantes.enColar(p);
    return true;
        
    }
    
    
}
